function onLoad() {
    if(window.location.search.length > 0) {
        fetch('/get/price' + window.location.search)
        .then(res => res.json())
        .then(json => {
            if(json.length > 0) {
                console.log(json)
                listFromItems(json)
            }
        })
    }
}

/**
 * Will iterate all items and render then with a delay.
 * @param {} items 
 */
function listFromItems(items) {
    
    let time = 1
    let itemCounter = 0
    const products = []
    items.forEach(item => {
        setTimeout(() => {
            products.push(item)
            compareAndRender(products)
            itemCounter++
            if(itemCounter >= items.length) {
                loader.style.display = 'none'
            } 
        },time)
        time += Math.random()*2000  
    })
}
/**
 * Only used to make the sort visible in real-time.
 * @param {} products 
 */
function compareAndRender(products) {
    const list = document.getElementById('item-holder')
    products.sort((a,b) => comparePrices(a,b))
    list.innerHTML = ''
    products.forEach(product => {
        list.innerHTML += getLi(product)
    })
}
function getLi(product) {
    return `
    <li class="list-group-item">
        <span class="listText heavy">${product.company}</span>
        <span class="listText floatRight">${notEmpty(trimPrice(product.price))}kr</span>
        <span class="listText name">${notEmpty(product.name)}</span>
    </li>`
}

function trimPrice(price) {
    const index = price.indexOf('.')
    return price.substring(0, index > 0 ? index : price.length)
}

function notEmpty(string) {
    return string === '' ? '---' : string
}
function comparePrices(a,b) {
    if(a.price === '') {
        return 1
    }
    else if(b.price === '') {
        return -1
    }
    return Number(a.price) - Number(b.price)
}