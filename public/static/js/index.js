function fetchPrices() {
    window.location.href = '/search?term=' + input.value
}
/**
 * Loads all the previous searches
 */
function onLoad() {
    fetch('/get/searcheditems')
    .then(res => res.json())
    .then(json => {
        let count = 1
        let length = json.searchedItems.length
        json.searchedItems.forEach(item => {
            previouslySearched.innerHTML += aTagFromJson(item) + (count < length ? ', ':'')
            count++
        })
    })
}

function aTagFromJson(item) {
    return `<a href="/search?term=${item}">${item}</a>`
}