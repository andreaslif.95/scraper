
const {getData} = require('./util/scrapers')
const {addSearchedItem, getSearchedItems} = require('./util/lastsearch')
const express = require('express')
const app = express()
const path = require('path')

const {PORT = 8080} = process.env

app.use('/assets', express.static(path.join(__dirname, 'public', 'static')))

app.get('/', (req, res) => {
    return res.status(200).sendFile(path.join(__dirname, 'public', 'index.html'))
})

app.get('/search', (req, res) => {
    return res.status(200).sendFile(path.join(__dirname, 'public', 'itemlist.html'))
})

app.get('/get/searcheditems', async (req, res) => {
    return res.status(200).json(await getSearchedItems())
})
app.get('/get/price', (req, res) => {
    //If no query we don't want to try to fetch.
    if(req.query != null && req.query.term != null) {
        getData(req.query.term).then((products) => {
           res.status(200).json(products)
           addSearchedItem(req.query.term)
           return
        })
        
    }
    else {
        return res.status(400)
    }
})

app.listen(PORT)