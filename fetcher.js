const fetch = require('node-fetch')

async function getHTML(url) {
    return new Promise((resolve, reject) =>{
        fetch(url)
            .then((resp) => resolve(resp.text()))
            .catch(err => reject(err))
    })
}

module.exports = {getHTML}