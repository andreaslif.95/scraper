const fs = require('fs')
const fileDest = __dirname + '/lastSearched.json'
/**
 * Adds the last searched item to the json object.
 * @param {} query 
 */
function addSearchedItem(query) {
    console.log(fileDest)
    fs.readFile(fileDest, 'utf-8', async (err, content) => {
        console.log(content)
        const json = await JSON.parse(content)
        const items = json.searchedItems
        items.unshift(query)
        if(items.length > 3)
        items.pop()
        
        fs.writeFileSync(fileDest, JSON.stringify(json))
    })   
} 

async function getSearchedItems() {
    return await JSON.parse(fs.readFileSync(fileDest))
}

module.exports = {addSearchedItem, getSearchedItems}