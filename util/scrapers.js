const {getHTML} = require('../fetcher')
const Cheerio = require('cheerio')

function netonNet(product) {
    console.log(product)
    return new Promise((resolve, reject) => {
        product.split('?').join()
        getHTML('https://www.netonnet.se/Search?query='+product.split(' ').join('+'))
            .then((html) => {
                const $ = Cheerio.load(html)
                let firstProduct = $('#productList > div').children()[0]
                firstProduct = $(firstProduct).find('input')
                const product = {company: 'NetOnNet'}
                for(let i =0; i < firstProduct.length; i++) {
                    const data = $(firstProduct[i])
                    const name = data.attr('name')
                    if(name.toLowerCase().includes("price")) {
                        product.price =data.val()
                        product.price = product.price.substring(0, product.price.indexOf(','))
                    }
                    else if(name.toLowerCase().includes("name")) {
                        product.name =data.val()
                    }
                }
                resolve(product)

            })
            .catch(err => reject(err))
        })
}

function inet(product) {
    return new Promise((resolve, reject) => {
        product.split('?').join()
        getHTML('https://www.inet.se/hitta?q='+product.split(' ').join('+'))
            .then((html) => {
                const $ = Cheerio.load(html)
                const firstProduct = $('.product-list > ul').children()[0]
                
                const name = $(firstProduct).find('h4').text()
                let price = $(firstProduct).find('.price')
                if(price.length > 1) {
                    price = $(price.get(1)).text().replace('kr', '').replace(/\s/, '').trim()
                }
                else {
                    price = price.text().replace('kr', '').replace(/\s/, '').trim()
                }
                
                console.log(price)
                resolve({company: 'Inet', name, price})

            })
            .catch(err => reject(err))
        })
}


function mediaMarkt(product) {
    return new Promise((resolve, reject) => {
        product.split('?').join()
        getHTML('https://www.mediamarkt.se/sv/search.html?query='+product.split(' ').join('+'))
            .then((html) => {
                const $ = Cheerio.load(html)
                const firstProduct = $('.products-list').find('script')[0]
                let json = $(firstProduct).html()
                json = json.substring(json.indexOf('{'), json.length -1)
                json = JSON.parse(json)
                resolve({company: 'MediaMarkt', name: json.name, price: json.price})

            })
            .catch(err => reject(err))
        })
}

function komplett(product) {
    return new Promise((resolve, reject) => {
        product.split('?').join()
        getHTML('https://www.komplett.se/search?q='+product.split(' ').join('+'))
            .then((html) => {
                const $ = Cheerio.load(html)
                const firstProduct = $('.product-list').find('.product-list-item')[0]
                const name = $(firstProduct).find('.text-content').find('h2').text()
                const price = $(firstProduct).find('.product-price-now').text().replace(/:-/, '').replace(/\s/, '').trim()
                resolve({company:'Komplett', name, price})

            })
            .catch(err => reject(err))
        })
}
/**
 * Iterates all scrapers and adds the product data tot he products array.
 * @param {} productName 
 */
async function getData(productName) {
    return new Promise((resolve, reject) => {
        const funcs = [netonNet, inet, mediaMarkt, komplett]
        const products = []
        try {
            for(let i = 0; i < funcs.length; i++) {
                funcs[i](productName).then(product => {
                    products.push(product)
                    if(products.length === 4) {
                        resolve(products)
                    }
                })
            }
        }
        catch(err) {
            reject(err)
        }
    })
}
module.exports = {getData}